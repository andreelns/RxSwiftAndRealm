//
//  DogStore.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RealmSwift
import RxCocoa
import RxSwift



class DogStore {
    
    enum DogField: String{
        case name="name", age="age", none=""
    }
    
    public static let instance = DogStore()
    
    let realmApp = RealmApplication.instance
    
    private init (){}
    
    func save(_ dog: Dog) -> Void {
        try! realmApp.realm.write {
            realmApp.realm.add(dog)
        }
        
    }
    
    func getDogs(by dogField: DogField = .none, ascending: Bool = true) -> Observable<Results<Dog>> {
        
        var dogs = realmApp.realm.objects(Dog.self)
        
        switch dogField {
        case .none:
            break
        default:
            dogs = dogs.sorted(byKeyPath: dogField.rawValue, ascending: ascending)
        }
        
        return Observable.collection(from: dogs)
    }
    
}

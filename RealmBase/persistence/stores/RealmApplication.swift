//
//  RealmConfig.swift
//  RealmBase
//
//  Created by André Lucas Silva on 05/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//


import UIKit
import RealmSwift
import RxCocoa
import RxSwift

class RealmApplication {
    
    public static let instance: RealmApplication = RealmApplication()
    
    private var _realm: Realm
    
    public var realm: Realm { return _realm }
    
    private init() {
        let config = Realm.Configuration( shouldCompactOnLaunch: {
            totalBytes, usedBytes in
            // totalBytes refers to the size of the file on disk in bytes (data + free space)
            // usedBytes refers to the number of bytes used by data in the file
            
            // Compact if the file is over 100MB in size and less than 50% 'used'
            let oneHundredMB = 100 * 1024 * 1024
            return (totalBytes > oneHundredMB) && (Double(usedBytes) / Double(totalBytes)) < 0.5
        })
        
        _realm = try! Realm(configuration: config)
        
    }

    
}

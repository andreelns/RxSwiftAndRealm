//
//  Person.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit

protocol PersonProtocol: Object {
    var name: String {get set}
    var picture: Data? {get set}
    var dogs: [DogProtocol] { get }
}

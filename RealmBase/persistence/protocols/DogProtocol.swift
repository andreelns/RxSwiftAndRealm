//
//  Dog.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit

protocol DogProtocol: Object {
    var name: String { get set}
    var age: Int { get set}
}

//
//  PersonPersistence.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RealmSwift

class Person: Object{
    @objc dynamic var name: String = ""
    @objc dynamic var picture: Data? = nil
    let dogs = [Dog]()
}

//
//  DogPersistence.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RealmSwift

class Dog: Object{
    @objc dynamic var name: String = "";
    @objc dynamic var age: Int = 0;
    
    override static func primaryKey() -> String {
        return "name"
    }
}

//
//  DogViewModel.swift
//  RealmBase
//
//  Created by André Lucas Silva on 04/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RealmSwift
import RxCocoa
import RxSwift

class DogViewModel {
    
    public let name = BehaviorRelay<String>(value: "")
    public let age = BehaviorRelay<String>(value: "0")
    public let isValid: Observable<Bool>
    
    
    // MARK: Private properties
    private let privateDataSource: BehaviorRelay<[String]> = BehaviorRelay(value: [])
    
    // MARK: Outputs
    public let dataSource: Observable<Results<Dog>>
    
    let store = DogStore.instance
    let bag = DisposeBag()
    
    init(addItemTap: Driver<Void>) {
        
        isValid = Observable.combineLatest(self.name.asObservable(), self.age.asObservable())        { (name, age) in
            
            var val = false
            
            if let age = Int(age){
            
                val = (name.count > 0) && (Int(age) > 0)
                
            }
            return val
        }
        
        // Make the output dataSource an Observable of the privateDataSource
        self.dataSource = store.getDogs(by: .name, ascending: false)
        
        //Register addButton tap to append a new "Item" to the dataSource on each tap -> onNext
        addItemTap.drive(onNext: { [unowned self] _ in
            
            let name = self.name.value
            
            if let age = Int(self.age.value){
                
                let dog = Dog()
                
                dog.age = age
                dog.name = name
                
                self.store.save(dog)
            }
           
        }).disposed(by: bag)
        
        
    }
    
}

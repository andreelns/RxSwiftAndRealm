//
//  ViewController.swift
//  RealmBase
//
//  Created by André Lucas Silva on 03/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DogViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel: DogViewModel!
    
    private let cellIdentifier = "DogCell"
    private let oldCellIdentifier = "DogVelhoCell"
    private let bag = DisposeBag()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupTextFields()
        setupAddButton()
        setupTableViewBinding()
    }
    
    private func setupViewModel() {
        self.viewModel = DogViewModel(addItemTap: addButton.rx.tap.asDriver())
    }

    private func setupTextFields(){
        self.nameTextField.rx.text.orEmpty.bind(to: viewModel.name).disposed(by: bag)
        self.ageTextField.rx.text.orEmpty.bind(to: viewModel.age).disposed(by: bag)
    }
    
    private func setupAddButton(){
        self.viewModel.isValid.map { $0 }
            .bind(to: addButton.rx.isEnabled)
            .disposed(by: self.bag)
    }
    
    private func setupTableView() {
        
        //This is necessary since the UITableViewController automatically set his tableview delegate and dataSource to self
        tableView.delegate = nil
        tableView.dataSource = nil
        
        tableView.tableFooterView = UIView() //Prevent empty rows
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: oldCellIdentifier)
    }
    
    private func setupTableViewBinding() {
        
        viewModel.dataSource
            .bind(to: tableView.rx.items(cellIdentifier: cellIdentifier, cellType: UITableViewCell.self)) {  row, dog, cell in
                
                if dog.age <= 10 {
                    cell.textLabel?.text = "\(dog.name)"
                    cell.detailTextLabel?.text = "\(dog.age)"
                }
            }
            .disposed(by: self.bag)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}




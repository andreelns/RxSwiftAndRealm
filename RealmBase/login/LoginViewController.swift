//
//  LoginViewController.swift
//  RealmBase
//
//  Created by André Lucas Silva on 05/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    private var viewModel: LoginViewModel!
    
    private let bag = DisposeBag()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupTextFields()
    }
    
    private func setupViewModel() {
        self.viewModel = LoginViewModel(loginTap: loginButton.rx.tap.asDriver())
    }
    
    private func setupTextFields(){
        self.usernameTextField.rx.text.orEmpty.bind(to: viewModel.username).disposed(by: bag)
    }
}

//
//  LoginViewModel.swift
//  RealmBase
//
//  Created by André Lucas Silva on 05/05/2018.
//  Copyright © 2018 AndreeLNS. All rights reserved.
//

import UIKit
import RealmSwift
import RxCocoa
import RxSwift

class LoginViewModel {
    
    public let username = BehaviorRelay<String>(value: "")
    
    let store = DogStore.instance
    let bag = DisposeBag()
    
    init(loginTap: Driver<Void>) {
        
        //Register addButton tap to append a new "Item" to the dataSource on each tap -> onNext
        loginTap.drive(onNext: { [unowned self] _ in
            
            if self.username.value.count > 0 {
                print(self.username.value)
            }
            
        }).disposed(by: bag)
        
    }
}
